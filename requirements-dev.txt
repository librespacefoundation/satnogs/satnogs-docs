doc8~=0.11.0
sphinx-autobuild==2024.4.16
sphinx-lint~=0.9.0
tox~=4.13.0
vale~=3.0.0.0
