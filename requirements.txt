Sphinx~=7.2.0
pypandoc~=1.12.0
python-gitlab~=3.15.0
sphinx-rtd-theme~=1.3.0
versioneer~=0.29
