##################
Project Principles
##################

The SatNOGS project embodies the principles laid out in the Libre Space
Manifesto, which represents a set of ideals focusing on the open and unimpeded
access to space technology and data.

***********
Open source
***********

Every piece of software and hardware developed for SatNOGS, is open source
under copyleft licenses.
This ensures that anyone can use, modify, and redistribute the software,
and that all modified versions of the licensed software remain free and
open.

*********
Open data
*********

All data are freely accessible to anyone under a license which ensures that
attribution is given to the project and that any derived works can only be
distributed under the same license.

****************
Open development
****************

The development processes in the project are transparent, allowing anyone to
see how decisions are made, how technologies are developed, and how they are
eventually deployed.
These processes are properly documented and include details about the
technology’s design, functionality, deployment, and operation.
This ensures that knowledge is preserved, shared, and can be built upon by
others beyond the original development team.
